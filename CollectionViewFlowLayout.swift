//
//  CollectionViewFlowLayout.swift
//  CollectionViewFlowLayout
//
//  Created by Alexander Nikolaenko on 20.12.2018.
//  Copyright © 2018 Alexander Nikolaenko. All rights reserved.
//

import Foundation

protocol CollectionViewFlowLayoutDelegate: AnyObject {
    func collectionView(_ collectionView: UICollectionView, widthForLabelAtIndexPath indexPath: IndexPath) -> CGFloat
}

struct CollectionViewFlowLayoutOptions {
    var indentBetweenlabelsByHorizontal: CGFloat = 0
    var indentBetweenlabelsByVertical: CGFloat = 0
    var insetLeftRightLabel: CGFloat = 0
    var heightOfItem: CGFloat = 0
}

class CollectionViewFlowLayout: UICollectionViewFlowLayout {

    weak var delegate: CollectionViewFlowLayoutDelegate?
    
    private var options = CollectionViewFlowLayoutOptions()
    private var cache = [UICollectionViewLayoutAttributes]()
    
    private var numberOfRows: Int {
        guard let collectionView = collectionView else {
            return 0
        }
        
        let height = collectionView.bounds.height
        let countOfLabels = CGFloat(Int(height / options.heightOfItem))
        let heigthOfIndents = options.indentBetweenlabelsByVertical * (countOfLabels - 1)
        let heigthOfLabelsWithIndent = (countOfLabels * options.heightOfItem + heigthOfIndents) / countOfLabels
        let countOfLabelsWithIndent = Int(height / heigthOfLabelsWithIndent)
        
        return countOfLabelsWithIndent
    }
    
    private var contentWidth: CGFloat = 0
    private var contentHeight: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        return collectionView.bounds.height
    }
    
    // MARK: -
    
    init(_ options: CollectionViewFlowLayoutOptions) {
        super.init()
        self.options = options
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: -

    override var collectionViewContentSize: CGSize {
        return .init(width: contentWidth + options.indentBetweenlabelsByHorizontal, height: contentHeight)
    }

    override func prepare() {
        guard cache.isEmpty == true, let collectionView = collectionView else {
            return
        }

        let rowHeight = contentHeight / CGFloat(numberOfRows)
        var yOffset = [CGFloat]()
        for row in 0 ..< numberOfRows {
            yOffset.append(CGFloat(row) * rowHeight)
        }

        var row = 0
        var xOffset = [CGFloat](repeating: options.indentBetweenlabelsByHorizontal, count: numberOfRows)

        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {

            let indexPath = IndexPath(item: item, section: 0)

            var width: CGFloat = delegate?.collectionView(collectionView,
                                                          widthForLabelAtIndexPath: indexPath) ?? itemSize.width
            width += options.insetLeftRightLabel

            let frame = CGRect(x: xOffset[row],
                               y: yOffset[row],
                               width: width,
                               height: options.heightOfItem)
            let insetFrame = frame

            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)

            contentWidth = max(contentWidth, frame.maxX)
            xOffset[row] = xOffset[row] + width + options.indentBetweenlabelsByHorizontal

            row = row < (numberOfRows - 1) ? (row + 1) : 0
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()

        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
}

// MARK: - Public methods

extension CollectionViewFlowLayout {
    func update() {
        cache.removeAll()
        invalidateLayout()
    }
}
